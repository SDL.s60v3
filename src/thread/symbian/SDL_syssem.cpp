/*
   SDL - Simple DirectMedia Layer
   Copyright (C) 1997, 1998, 1999, 2000  Sam Lantinga

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this library; if not, write to the Free
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

   Sam Lantinga
   slouken@devolution.com
   */

#include <e32std.h>
#include "SDL_error.h"
#include "SDL_thread.h"

#define SDL_MUTEX_TIMEOUT -2

struct SDL_semaphore
{
	int handle;
	int count;
};

extern int CreateUnique(int (*aFunc)(const TDesC& aName, void*, void*), void*, void*);

int NewSema(const TDesC& aName, void* aPtr1, void* aPtr2) 
{
	int value = *((int*) aPtr2);
	return ((RSemaphore*)aPtr1)->CreateGlobal(aName, value);
}

/* Create a semaphore */
SDL_sem *SDL_CreateSemaphore(Uint32 initial_value)
{
	RSemaphore s;
	int status = CreateUnique(NewSema, &s, &initial_value);
	if(status != KErrNone)
	{
		SDL_SetError("Couldn't create semaphore");
	}
	SDL_semaphore* sem = new SDL_semaphore;
	sem->handle = s.Handle();
	sem->count = initial_value;
	return(sem);
}

/* Free the semaphore */
void SDL_DestroySemaphore(SDL_sem *sem)
{
	if ( sem )
	{
		RSemaphore sema;
		sema.SetHandle(sem->handle);
		sema.Signal(sem->count);
		sema.Close();
		delete sem;
		sem = NULL;
	}
}

void _WaitAll(SDL_sem *sem)
{
	//since SemTryWait may changed the counter.
	//this may not be atomic, but hopes it works.
	RSemaphore sema;
	sema.SetHandle(sem->handle);
	sema.Wait();
	while(sem->count < 0)
	{
		sema.Wait();
	}    
}

int SDL_SemWaitTimeout(SDL_sem *sem, Uint32 timeout)
{
	if ( ! sem ) {
		SDL_SetError("Passed a NULL sem");
		return -1;
	}

	if ( timeout == SDL_MUTEX_MAXWAIT )
	{
		_WaitAll(sem);
		return SDL_MUTEX_MAXWAIT;
	} 

	RSemaphore sema;
	sema.SetHandle(sem->handle);
	if(KErrNone == sema.Wait(timeout))
		return 0;
	return -1;
}

int SDL_SemTryWait(SDL_sem *sem)
{
	if(sem->count > 0)
	{
		sem->count--;
	}
	return SDL_MUTEX_TIMEOUT;
}

int SDL_SemWait(SDL_sem *sem) 
{
	return SDL_SemWaitTimeout(sem, SDL_MUTEX_MAXWAIT);
}

/* Returns the current count of the semaphore */
Uint32 SDL_SemValue(SDL_sem *sem)
{
	if ( ! sem ) {
		SDL_SetError("Passed a NULL sem");
		return 0;
	}
	return sem->count;
}

int SDL_SemPost(SDL_sem *sem)
{
	if ( ! sem ) {
		SDL_SetError("Passed a NULL sem");
		return -1;
	}
	sem->count++;
	RSemaphore sema;
	sema.SetHandle(sem->handle);
	sema.Signal();
	return 0;
}
