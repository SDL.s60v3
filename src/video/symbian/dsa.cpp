#include "dsa.h"
#include "sdlepocapi.h"
#include <cdsb.h>
#include <basched.h>

class CDsaGles : public CDsa
{
	public:
		CDsaGles(RWsSession& aSession) : CDsa(aSession) {}

	private:
		TUint8* LockSurface() { return NULL; }
		void UnlockHwSurface() {}
		void CreateSurfaceL() {}
};

//////////////////////////////////////////////////////////////////////

class CDsaBase : public CDsa, public MDirectScreenAccess
{
	public:
		CDsaBase(RWsSession& aSession) : CDsa(aSession) { m_updateWholeScreen = false; }
		~CDsaBase();

	private:
		void ConstructL(RWindow& aWindow, CWsScreenDevice& aDevice);
		void Stop();

		void AbortNow(RDirectScreenAccess::TTerminationReasons aReason);
		void Restart(RDirectScreenAccess::TTerminationReasons aReason);
		void RestartL();

		void Free();

		TUint8* LockSurface();
		void UnlockHwSurface();
		void CreateSurfaceL();

		CDirectScreenAccess*  iDsa;
};

void CDsaBase::ConstructL(RWindow& aWindow, CWsScreenDevice& aDevice)
{
	CDsa::ConstructL(aWindow, aDevice);
	if(iDsa != NULL)
	{
		iDsa->Cancel();
		delete iDsa;
		iDsa = NULL;
	}

	iDsa = CDirectScreenAccess::NewL(Session(), aDevice, aWindow, *this);
	RestartL();
}

CDsaBase::~CDsaBase()
{
	if(iDsa != NULL)
	{
		iDsa->Cancel();
	}
	delete iDsa;
}

void CDsaBase::RestartL()
{
	iDsa->StartL();
	iDsa->Gc()->SetClippingRegion(iDsa->DrawingRegion());

	Start();
}

void CDsaBase::Free()
{
	delete iBmp;
	iBmp = NULL;
}

TUint8* CDsaBase::LockSurface()
{
	iBmp->LockHeap();
	return (TUint8*)iBmp->DataAddress();
}

void CDsaBase::UnlockHwSurface()
{
	iBmp->UnlockHeap();
	iDsa->Gc()->BitBlt(TPoint(0, 0), iBmp);
	iDsa->ScreenDevice()->Update();
}

void CDsaBase::CreateSurfaceL()
{
	Free();
	iBmp = new CFbsBitmap();
	User::LeaveIfError(iBmp->Create(Window()->Size(), EColor64K));
}

void CDsaBase::AbortNow(RDirectScreenAccess::TTerminationReasons /*aReason*/)
{
	Stop();
}

void CDsaBase::Restart(RDirectScreenAccess::TTerminationReasons /*aReason*/)
{
	TRAPD(err, RestartL());
	if(err == KLeaveExit)
	{
		Stop();
	}
	else
	{
		PANIC_IF_ERROR(err);
	}
}

void CDsaBase::Stop()
{
	CDsa::Stop();
	iDsa->Cancel();
}


CDsa* CDsa::CreateL(RWsSession& aSession)
{
	return new CDsaBase(aSession);
}

void CDsa::Free()
{
}

CDsa::~CDsa()
{
	delete[] iLut256;
}

void CDsa::ConstructL(RWindow& aWindow, CWsScreenDevice& /*aDevice*/)
{
	if(iLut256 == NULL)
		iLut256 = new TUint32[256];
	iWindow = &aWindow;
}

int CDsa::SetPalette(int aFirst, int aCount, TUint32* aPalette)
{
	if(iLut256 == NULL)
		return KErrNotFound;
	for(int i = aFirst; i < aFirst + aCount; i++)
	{
		iLut256[i] = aPalette[i];
	}
	return KErrNone;
}

CDsa::CDsa(RWsSession& aSession) :
	iBmp(NULL),
	iRunning(false),
	iSession(aSession)
{
}

RWsSession& CDsa::Session()
{
	return iSession;
}

int CDsa::AllocSurface(int bpp)
{
	iSourceBpp = bpp;

	TRAPD(err, CreateSurfaceL());
	return err;
}

bool CDsa::Blit(const TUint8* aBits, const TSize& aSize)
{
	TUint16* target = (TUint16*)LockSurface();
	if(target == NULL)
		return false;

	TSize ss = iBmp->SizeInPixels();

	if( ss != aSize )
	{
		memset( target, 0, ss.iWidth * ss.iHeight * 2 );
	}

	if( aSize.iWidth <= ss.iWidth && aSize.iHeight <= ss.iHeight )
	{
		TUint16* dst = target
			+ ( ( ss.iHeight - aSize.iHeight ) / 2 ) * ss.iWidth
			+ ( ss.iWidth - aSize.iWidth ) / 2;
		int skip = ss.iWidth - aSize.iWidth;

		unsigned int h = aSize.iHeight;

		switch( iSourceBpp )
		{
			case 1:
			{
				TUint8* src = (TUint8*)aBits;
				do
				{
					unsigned int w = aSize.iWidth;
					do
					{
						*dst++ = iLut256[*src++];
					}
					while( --w );
					dst += skip;
				}
				while( --h );
			}
			break;

			case 2:
			{
				TUint16* src = (TUint16*)aBits;
				do
				{
					memcpy( dst, src, aSize.iWidth * 2 );
					dst += ss.iWidth;
					src += aSize.iWidth;
				}
				while( --h );
			}
			break;

			case 4:
			{
				TUint32* src = (TUint32*)aBits;
				do
				{
					unsigned int w = aSize.iWidth;
					do
					{
						*dst++ =
							( ( *src & 0xF80000 ) >> 8 ) |
							( ( *src & 0x00FC00 ) >> 5 ) |
							( ( *src & 0x0000F8 ) >> 3 );
						src++;
					}
					while( --w );
					dst += skip;
				}
				while( --h );
			}
			break;

			default:
			break;
		}
	}
	else
	{
		TUint16* dst = target
			+ ( ( ss.iHeight - aSize.iHeight / 2 ) / 2 ) * ss.iWidth
			+ ( ss.iWidth - aSize.iWidth / 2 ) / 2;
		int skip = ss.iWidth - aSize.iWidth / 2;

		unsigned int h = aSize.iHeight / 2;

		switch( iSourceBpp )
		{
			case 1:
			{
				TUint8* src1 = (TUint8*)aBits;
				TUint8* src2 = (TUint8*)aBits + aSize.iWidth;
				do
				{
					unsigned int w = aSize.iWidth / 2;
					do
					{
						TUint32 c1 = iLut256[*src1++];
						TUint32 c2 = iLut256[*src1++];
						TUint32 c3 = iLut256[*src2++];
						TUint32 c4 = iLut256[*src2++];

						c1 = ( c1 & 0xF81F ) | ( ( c1 & 0x07E0 ) << 16 );
						c2 = ( c2 & 0xF81F ) | ( ( c2 & 0x07E0 ) << 16 );
						c3 = ( c3 & 0xF81F ) | ( ( c3 & 0x07E0 ) << 16 );
						c4 = ( c4 & 0xF81F ) | ( ( c4 & 0x07E0 ) << 16 );

						TUint32 c = ( c1 + c2 + c3 + c4 ) / 4;
						*dst++ = ( c & 0xF81F ) | ( ( c & 0x07E00000 ) >> 16 );
					}
					while( --w );
					dst += skip;
					src1 += aSize.iWidth;
					src2 += aSize.iWidth;
				}
				while( --h );
			}
			break;

			case 2:
			{
				TUint16* src1 = (TUint16*)aBits;
				TUint16* src2 = (TUint16*)aBits + aSize.iWidth;
				do
				{
					unsigned int w = aSize.iWidth / 2;
					do
					{
						TUint32 c1 = ( *src1 & 0xF81F ) | ( ( *src1 & 0x07E0 ) << 16 );
						src1++;
						TUint32 c2 = ( *src1 & 0xF81F ) | ( ( *src1 & 0x07E0 ) << 16 );
						src1++;
						TUint32 c3 = ( *src2 & 0xF81F ) | ( ( *src2 & 0x07E0 ) << 16 );
						src2++;
						TUint32 c4 = ( *src2 & 0xF81F ) | ( ( *src2 & 0x07E0 ) << 16 );
						src2++;

						TUint32 c = ( c1 + c2 + c3 + c4 ) / 4;
						*dst++ = ( c & 0xF81F ) | ( ( c & 0x07E00000 ) >> 16 );
					}
					while( --w );
					dst += skip;
					src1 += aSize.iWidth;
					src2 += aSize.iWidth;
				}
				while( --h );
			}
			break;

			case 4:
			{
				TUint32* src1 = (TUint32*)aBits;
				TUint32* src2 = (TUint32*)aBits + aSize.iWidth;
				do
				{
					unsigned int w = aSize.iWidth / 2;
					do
					{
						TUint32 c1 = 	( ( *src1 & 0xF80000 ) >> 8  ) |
								( ( *src1 & 0x00FC00 ) << 11 ) |
								( ( *src1 & 0x0000F8 ) >> 3  );
						src1++;
						TUint32 c2 = 	( ( *src1 & 0xF80000 ) >> 8  ) |
								( ( *src1 & 0x00FC00 ) << 11 ) |
								( ( *src1 & 0x0000F8 ) >> 3  );
						src1++;
						TUint32 c3 = 	( ( *src2 & 0xF80000 ) >> 8  ) |
								( ( *src2 & 0x00FC00 ) << 11 ) |
								( ( *src2 & 0x0000F8 ) >> 3  );
						src2++;
						TUint32 c4 = 	( ( *src2 & 0xF80000 ) >> 8  ) |
								( ( *src2 & 0x00FC00 ) << 11 ) |
								( ( *src2 & 0x0000F8 ) >> 3  );
						src2++;

						TUint32 c = ( c1 + c2 + c3 + c4 ) / 4;
						*dst++ = ( c & 0xF81F ) | ( ( c & 0x07E00000 ) >> 16 );
					}
					while( --w );
					dst += skip;
					src1 += aSize.iWidth;
					src2 += aSize.iWidth;
				}
				while( --h );
			}
			break;

			default:
			break;
		}
	}

	UnlockHwSurface();

	return true;
}

void CDsa::Stop()
{
	iRunning = false;
}

void CDsa::Start()
{
	iRunning = true;
}

RWindow* CDsa::Window()
{
	return iWindow;
}

CDsa* CDsa::CreateGlesDsaL()
{
	CDsa* dsa = new CDsaGles(Session());
	CWsScreenDevice* dummy = NULL;
	dsa->ConstructL(*Window(), *dummy);
	Free();
	delete this;
	return dsa;
}
