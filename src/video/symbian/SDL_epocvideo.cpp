/*
   SDL - Simple DirectMedia Layer
   Copyright (C) 1997, 1998, 1999, 2000, 2001  Sam Lantinga

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this library; if not, write to the Free
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

   Sam Lantinga
   slouken@devolution.com
   */

#include "epoc_sdl.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

extern "C" {
#include "SDL_error.h"
#include "SDL_timer.h"
#include "SDL_video.h"
#undef NULL
#include "SDL_pixels_c.h"
#include "SDL.h"
#include "SDL_mouse.h"
}

#include "SDL_epocvideo.h"
#include "SDL_epocevents_c.h"
#include <coedef.h>
#include <flogger.h>
#include <eikenv.h>
#include <eikappui.h>
#include <eikapp.h>
#include "sdlepocapi.h"
#include <SDL_gliop.h>
#include <egl.h>
#include "gles_armv5_def.h"

extern "C"
{
	/* Initialization/Query functions */
	static int S60_VideoInit(_THIS, SDL_PixelFormat *vformat);
	static SDL_Rect **S60_ListModes(_THIS, SDL_PixelFormat *format, Uint32 flags);
	static SDL_Surface *S60_SetVideoMode(_THIS, SDL_Surface *current, int width, int height, int bpp, Uint32 flags);
	static int S60_SetColors(_THIS, int firstcolor, int ncolors, SDL_Color *colors);
	static void S60_VideoQuit(_THIS);

	/* Hardware surface functions */
	static int S60_AllocHWSurface(_THIS, SDL_Surface *surface);
	static int S60_LockHWSurface(_THIS, SDL_Surface *surface);
	static int S60_FlipHWSurface(_THIS, SDL_Surface *surface);
	static void S60_UnlockHWSurface(_THIS, SDL_Surface *surface);
	static void S60_FreeHWSurface(_THIS, SDL_Surface *surface);
	static void S60_DirectUpdate(_THIS, int numrects, SDL_Rect *rects);

	static int S60_Available(void);
	static SDL_VideoDevice *S60_CreateDevice(int devindex);

	/* Mouse functions */
	static WMcursor *S60_CreateWMCursor(_THIS, Uint8 *data, Uint8 *mask, int w, int h, int hot_x, int hot_y);
	static void S60_FreeWMCursor(_THIS, WMcursor *cursor);
	static int S60_ShowWMCursor(_THIS, WMcursor *cursor);

	/*GL Functions*/
	static int S60_GlesLoadLibrary(_THIS, const char* path);
	static void* S60_GlesGetProcAddress(_THIS, const char *proc);
	static int S60_GlesGetAttribute(_THIS, SDL_GLattr attrib, int* value);
	static int S60_GlesMakeCurrent(_THIS);
	static void S60_GlesSwapBuffers(_THIS);

	struct WMcursor
	{
	};
}

static TSize GetScreenSize()
{
	CWsScreenDevice *sd = new CWsScreenDevice(CEikonEnv::Static()->WsSession());
	sd->Construct();
	const TSize sz(sd->SizeInPixels());
	delete sd;

	return sz;
}

/* Epoc video driver bootstrap functions */
static int S60_Available(void)
{
	return 1; /* Always available */
}

static void S60_DeleteDevice(SDL_VideoDevice *device)
{
	delete device->gl_data;
	delete device->hidden;
	delete device;
}

static SDL_VideoDevice *S60_CreateDevice(int /*devindex*/)
{
	SDL_VideoDevice *device;

	/* Allocate all variables that we free on delete */
	device = new SDL_VideoDevice;
	if (device) 
	{
		Mem::FillZ(device, (sizeof *device));
		device->hidden = new SDL_PrivateVideoData;
		device->gl_data = new SDL_PrivateGLData;
	}
	else
	{
		SDL_OutOfMemory();
		return 0;	
	}
	if((device->hidden == NULL) || (device->gl_data == NULL))
	{
		SDL_OutOfMemory();
		delete device->hidden;
		delete device->gl_data;
		delete device;
		return 0;
	}

	Mem::FillZ(device->hidden, (sizeof *device->hidden));

	/* Set the function pointers */
	device->VideoInit = S60_VideoInit;
	device->ListModes = S60_ListModes;
	device->SetVideoMode = S60_SetVideoMode;
	device->SetColors = S60_SetColors;
	device->UpdateRects = NULL;
	device->VideoQuit = S60_VideoQuit;
	device->AllocHWSurface = S60_AllocHWSurface;
	device->CheckHWBlit = NULL;
	device->FillHWRect = NULL;
	device->SetHWColorKey = NULL;
	device->SetHWAlpha = NULL;
	device->LockHWSurface = S60_LockHWSurface;
	device->UnlockHWSurface = S60_UnlockHWSurface;
	device->FlipHWSurface = S60_FlipHWSurface;
	device->FreeHWSurface = S60_FreeHWSurface;
	device->SetIcon = NULL;
	device->SetCaption = NULL;
	device->GetWMInfo = NULL;
	device->FreeWMCursor = S60_FreeWMCursor;
	device->CreateWMCursor = S60_CreateWMCursor;
	device->ShowWMCursor = S60_ShowWMCursor;
	device->WarpWMCursor = NULL;
	device->InitOSKeymap = EPOC_InitOSKeymap;
	device->PumpEvents = EPOC_PumpEvents;
	device->free = S60_DeleteDevice;

	/*gles funtions*/
	device->GL_LoadLibrary 		= S60_GlesLoadLibrary;
	device->GL_GetProcAddress	= S60_GlesGetProcAddress;
	device->GL_GetAttribute 	= S60_GlesGetAttribute;
	device->GL_MakeCurrent 		= S60_GlesMakeCurrent;
	device->GL_SwapBuffers 		= S60_GlesSwapBuffers;

	device->gl_data->iLibrary.SetHandle(0);

	return device;
}

VideoBootStrap EPOC_bootstrap = {
	"epoc\0\0\0", "EPOC system",
	S60_Available, S60_CreateDevice
};

int S60_VideoInit(_THIS, SDL_PixelFormat *vformat)
{
	/* Initialise Epoc frame buffer */

	/* The "best" video format should be returned to caller. */

	vformat->BitsPerPixel 	= 16;
	vformat->BytesPerPixel  = 2;

	Private->iRect = new SDL_Rect*[5];
	Private->iRect[0] = new SDL_Rect;
	Private->iRect[1] = new SDL_Rect;
	Private->iRect[2] = new SDL_Rect;
	Private->iRect[3] = new SDL_Rect;
	Private->iRect[4] = NULL;

	for( int i=0; i<4; i++ )
	{
		Private->iRect[i]->x = 0;
		Private->iRect[i]->y = 0;
	}

	const TSize sz = GetScreenSize();

	Private->iRect[0]->w = sz.iWidth;
	Private->iRect[0]->h = sz.iHeight;

	Private->iRect[1]->w = sz.iHeight;
	Private->iRect[1]->h = sz.iWidth;

	Private->iRect[2]->w = sz.iWidth * 2;
	Private->iRect[2]->h = sz.iHeight * 2;

	Private->iRect[3]->w = sz.iHeight * 2;
	Private->iRect[3]->h = sz.iWidth * 2;

	Private->iOrientation = new CAknAppUi::TAppUiOrientation[4];

	if(sz.iWidth < sz.iHeight)
	{
		Private->iOrientation[0] = CAknAppUi::EAppUiOrientationPortrait;
		Private->iOrientation[1] = CAknAppUi::EAppUiOrientationLandscape;
		Private->iOrientation[2] = CAknAppUi::EAppUiOrientationPortrait;
		Private->iOrientation[3] = CAknAppUi::EAppUiOrientationLandscape;
	}
	else
	{
		Private->iOrientation[0] = CAknAppUi::EAppUiOrientationLandscape;
		Private->iOrientation[1] = CAknAppUi::EAppUiOrientationPortrait;
		Private->iOrientation[2] = CAknAppUi::EAppUiOrientationLandscape;
		Private->iOrientation[3] = CAknAppUi::EAppUiOrientationPortrait;
	}

	return(0);
}

SDL_Rect **S60_ListModes(_THIS, SDL_PixelFormat *format, Uint32 flags)
{
	if(flags & SDL_HWSURFACE)
	{
		if(format->BytesPerPixel != 4) //in HW only full color is supported
			return NULL;
	}
	if(flags & SDL_FULLSCREEN)
	{
		return Private->iRect;
	}

	return (SDL_Rect **)(-1); //everythingisok, but too small shoes
}

int S60_SetColors(_THIS, int firstcolor, int ncolors, SDL_Color *colors)
{
	if ((firstcolor+ncolors) > 256)
		return -1;
	TUint32 palette[256];
	int c = 0;

	for(int i = firstcolor; i < firstcolor+ncolors; i++)
	{
		TUint32 color64K = (colors[c].r & 0x0000f8) << 8;
		color64K		|= (colors[c].g & 0x0000fc) << 3;
		color64K		|= (colors[c].b & 0x0000f8) >> 3;
		palette[i] = color64K;
		c++;
	}

	if(EpocSdlEnv::SetPalette(firstcolor, ncolors, palette) == KErrNone)
	{
		return 0;
	}
	return -1;
}

int S60_GlesLoadLibrary(SDL_VideoDevice* _this, const char* path)
{
	if(_this->gl_data->iLibrary.Handle() != 0)
		return KErrAlreadyExists; //already loaded
	const char* const gles_lib[] = {"libgles_cm.dll", "libSWGLES.dll"};
	int err = KErrNotFound;
	for(int i = 0; i < 2 && err != KErrNone; i++)
	{
		const char* name8 = path == NULL ? gles_lib[i] : path;
		TFileName lib;
		lib.Copy(TPtrC8((unsigned char*)name8));
		err = _this->gl_data->iLibrary.Load(lib); 
	}
	if(err == KErrNone)
		_this->gl_config.driver_loaded = 1;
	return err;
}

typedef int (*Ftp)(...);	
#define DC(x) ((Ftp) S60_GlesGetProcAddress(_this, #x))	

const char* const OpenGL[] = //these funtions are in gl, but not in gles, at least in all in all versions...
{
	"glBegin",
	"glEnd",
	"glOrtho",
	"glPopAttrib",
	"glPopClientAttrib",
	"glPushAttrib",
	"glPushClientAttrib",
	"glTexCoord2f",
	"glVertex2i",
	"glTexParameteri"
};

int NotSupported()
{
	User::Panic(_L("SDL, Gles"), KErrNotSupported);
	return 0;
}

void* S60_GlesGetProcAddress(_THIS, const char *proc)
{
	if(_this->gl_data->iLibrary.Handle() == 0)
		return NULL; //not loaded
	TLibraryFunction f = NULL;
	for(int i = 0; i < G_ordinals_count; i++)
	{
		if(strcmp(G_ordinals[i].name, proc) == 0)
		{
			f = _this->gl_data->iLibrary.Lookup(G_ordinals[i].ord);
			break;
		}
	}

	if(f != NULL) /*Lookup may fail*/
		return  (void*) f;	

	for(int i = 0; i < sizeof(OpenGL) / sizeof(char*); i++)
	{
		if(strcmp(OpenGL[i], proc) == 0)
			return (void*) NotSupported;
	}

	return NULL;
}

int S60_GlesGetAttribute(_THIS, SDL_GLattr aAttrib, int* aValue)
{
	EGLint attrib;
	switch(aAttrib)
	{
		/*todo*/
		case SDL_GL_RED_SIZE: attrib = EGL_RED_SIZE; break;
		case SDL_GL_GREEN_SIZE: attrib = EGL_GREEN_SIZE; break;
		case SDL_GL_BLUE_SIZE:attrib = EGL_BLUE_SIZE; break;
		case SDL_GL_ALPHA_SIZE: attrib = EGL_ALPHA_SIZE; break;
		case SDL_GL_BUFFER_SIZE: attrib = EGL_BUFFER_SIZE; break;
		case SDL_GL_DOUBLEBUFFER: *aValue = 1; return 0; //always
		case SDL_GL_DEPTH_SIZE: attrib = EGL_DEPTH_SIZE; break;
		case SDL_GL_STENCIL_SIZE: attrib = EGL_STENCIL_SIZE; break;
		case SDL_GL_ACCUM_RED_SIZE: 
		case SDL_GL_ACCUM_GREEN_SIZE:
		case SDL_GL_ACCUM_BLUE_SIZE:
		case SDL_GL_ACCUM_ALPHA_SIZE:
		case SDL_GL_STEREO:
		case SDL_GL_MULTISAMPLEBUFFERS:
		case SDL_GL_MULTISAMPLESAMPLES:
		case SDL_GL_ACCELERATED_VISUAL:
		case SDL_GL_SWAP_CONTROL:
					  *aValue = 0;
					  return -1;
	}
	const int success = DC(eglGetConfigAttrib)
		(
		 _this->gl_data->iDisplay,
		 _this->gl_data->iConfig,
		 attrib,
		 aValue);
	return success == EGL_FALSE ? -1 : 0;
}


int S60_GlesMakeCurrent(_THIS)
{
	DC(eglMakeCurrent)
		(_this->gl_data->iDisplay,
		 _this->gl_data->iSurface,
		 _this->gl_data->iSurface,
		 _this->gl_data->iContext);
	return DC(eglGetError)();
}

void S60_GlesSwapBuffers(_THIS)
{
	DC(eglSwapBuffers)(
			_this->gl_data->iDisplay,
			_this->gl_data->iSurface);
}

static void glAssert(_THIS)
{
	const EGLint err = DC(eglGetError)();
	if(err != EGL_SUCCESS)
	{
		User::Leave(err);
	}
}

static void CreateGles(_THIS, RWindow& aWindow, int bpp, SDL_PrivateGLData& aData)	
{		
	SDL_GL_LoadLibrary(NULL); //just if its not already loaded
	aData.iDisplay = DC(eglGetDisplay)(EGL_DEFAULT_DISPLAY);
	DC(eglInitialize)(aData.iDisplay, NULL, NULL);

	glAssert(_this);

	int configs = 0;
	EGLConfig* configList = NULL;
	int configSz = 0;
	DC(eglGetConfigs)(aData.iDisplay, configList, configSz, &configs);
	configSz = configs; 

	glAssert(_this);

	configList = new EGLConfig[configSz];

	int red, green, blue;	
	if(bpp == 16)
	{
		red = 5;
		green = 6;
		blue = 5;	
	}
	else
	{ 
		red = 8;
		green = 8;
		blue = 8;
	}

	const EGLint attribList[] = 
	{
		EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
		EGL_RED_SIZE, red,
		EGL_GREEN_SIZE, green, 
		EGL_BLUE_SIZE, blue,
		EGL_BUFFER_SIZE, EGL_DONT_CARE,
		EGL_DEPTH_SIZE, 8,
		EGL_NONE
	};

	DC(eglChooseConfig)(aData.iDisplay,
			attribList,
			configList,
			configSz,
			&configs);


	glAssert(_this);

	__ASSERT_ALWAYS(configs > 0, User::Invariant());

	aData.iConfig = configList[0];

	delete[] configList;

	aData.iContext = DC(eglCreateContext)(aData.iDisplay,
			aData.iConfig,
			EGL_NO_CONTEXT,
			NULL);

	glAssert(_this);

	aData.iSurface	= DC(eglCreateWindowSurface)(aData.iDisplay,
			aData.iConfig,
			&aWindow, 
			NULL);

	glAssert(_this);

}

static void DestroyGles(_THIS)
{
	if(	_this->gl_config.driver_loaded)
	{
		DC(eglMakeCurrent)(_this->gl_data->iDisplay, 
				EGL_NO_SURFACE,
				EGL_NO_SURFACE,
				EGL_NO_CONTEXT);
		DC(eglDestroySurface)(_this->gl_data->iDisplay, _this->gl_data->iSurface);
		DC(eglDestroyContext)(_this->gl_data->iDisplay, _this->gl_data->iContext);
		DC(eglTerminate)(_this->gl_data->iDisplay);
		_this->gl_data->iLibrary.Close();
		_this->gl_config.driver_loaded = 0;
	}
}

SDL_Surface *S60_SetVideoMode(_THIS, SDL_Surface *current, int width, int height, int bpp, Uint32 flags)
{
	if(flags & SDL_OPENGL)
	{
		current->flags |= SDL_OPENGL;
		current->w = width;
		current->h = height;

		RWindow* win = EpocSdlEnv::Window();
		EpocSdlEnv::ApplyGlesDsa();	

		CreateGles(_this, *win, bpp, *_this->gl_data);

		return current;
	}

	if(flags & SDL_HWSURFACE)
		return NULL;

	// check orientation and resolution validity
	bool resValid = false;
	Orientation orientation = CAknAppUi::EAppUiOrientationUnspecified;
	for(int i=0; i<4; i++)
	{
		if(width <= Private->iRect[i]->w && height <= Private->iRect[i]->h)
		{
			orientation = Private->iOrientation[i];
			resValid = true;
			break;
		}
	}
	if(!resValid)
		return NULL;

	if(!SDL_ReallocFormat(current, bpp, 0, 0, 0, 0))
		return NULL;

	const int numBytesPerPixel = ((bpp-1)>>3) + 1;
	current->pitch = numBytesPerPixel * width;

	// set proper surface flags
	current->flags = SDL_SWSURFACE | SDL_PREALLOC;
	if(flags & SDL_FULLSCREEN)
		current->flags |= SDL_FULLSCREEN;
	if(flags & SDL_RESIZABLE)
		current->flags |= SDL_RESIZABLE;
	if(bpp <= 8)
		current->flags |= SDL_HWPALETTE;

	current->w = width;
	current->h = height;

	// allocate surface
	const int surfacesize = width * height * numBytesPerPixel;
	Private->iSwSurfaceSize = TSize(width, height);

	delete[] (TUint8*)current->pixels;
	current->pixels = new TUint8[surfacesize];
	Private->iSwSurface = (TUint8*) current->pixels;

	if(current->pixels == NULL)
		return NULL;

	const TSize sz = GetScreenSize();

	if(sz.iWidth < sz.iHeight && orientation != CAknAppUi::EAppUiOrientationPortrait ||
	   sz.iWidth > sz.iHeight && orientation != CAknAppUi::EAppUiOrientationLandscape)
	{
		g_SDL->SetOrientation(orientation, current->format->BitsPerPixel / 8);
	}
	else
	{
		if((flags & SDL_RESIZABLE) == 0)
		{
			TRAPD(err, static_cast<CAknAppUi*>(CEikonEnv::Static()->EikAppUi())->SetOrientationL(orientation));
		}
		else
		{
			TRAPD(err, static_cast<CAknAppUi*>(CEikonEnv::Static()->EikAppUi())->SetOrientationL(CAknAppUi::EAppUiOrientationUnspecified));
		}

		const int err = EpocSdlEnv::AllocSurface(current->format->BitsPerPixel / 8);
		if(err != KErrNone)
			return NULL;
	}

	// Set the blit function
	_this->UpdateRects = S60_DirectUpdate;

	return current;
}

static int S60_AllocHWSurface(_THIS, SDL_Surface* surface)
{
	return KErrNone == EpocSdlEnv::AllocSurface(surface->format->BitsPerPixel / 8);
}

static void S60_FreeHWSurface(_THIS, SDL_Surface* /*surface*/)
{
}

static int S60_LockHWSurface(_THIS, SDL_Surface* surface)
{
	if(EpocSdlEnv::IsDsaAvailable())
	{
		TUint8* address = EpocSdlEnv::LockHwSurface();
		if(address != NULL)
		{
			surface->pixels = address;
			return 1;
		}
	}
	return 0;
}
static void S60_UnlockHWSurface(_THIS, SDL_Surface* /*surface*/)
{
	EpocSdlEnv::UnlockHwSurface();
}

static int S60_FlipHWSurface(_THIS, SDL_Surface* /*surface*/)
{
	return(0);
}

static void S60_DirectUpdate(_THIS, int numrects, SDL_Rect *rects)
{
	if(EpocSdlEnv::IsDsaAvailable())
	{
		if(Private->iSwSurface)
		{
			EpocSdlEnv::Blit(Private->iSwSurface, Private->iSwSurfaceSize);
		}
		if(!Private->iIsWindowFocused)
		{
			User::After(100000);
		}
	}
}

/* Note:  If we are terminated, this could be called in the middle of
   another SDL video routine -- notably UpdateRects.
   */
void S60_VideoQuit(_THIS)
{
	delete[] Private->iOrientation;

	delete Private->iRect[0];
	delete Private->iRect[1];
	delete[] Private->iRect;

	if(_this->gl_data)
		DestroyGles(_this);

	delete[] Private->iSwSurface;
	Private->iSwSurface = NULL;
	EpocSdlEnv::FreeSurface();
}


WMcursor *S60_CreateWMCursor(_THIS, Uint8* /*data*/, Uint8* /*mask*/, int /*w*/, int /*h*/, int /*hot_x*/, int /*hot_y*/)
{
	// prevents SDL from displaying standard cursor
	return (WMcursor*) 1;
}

void S60_FreeWMCursor(_THIS, WMcursor* /*cursor*/)
{
}

int S60_ShowWMCursor(_THIS, WMcursor *cursor)
{
	return true;
}

/*FOR GL comp*/

void glBegin(GLenum a) {}
void glEnd(void) {}
void glOrtho(GLdouble l, GLdouble r, GLdouble b, GLdouble t, GLdouble n, GLdouble f) {}
void glPopAttrib(void) {}
void glPopClientAttrib(void){}
void glPushAttrib(GLbitfield mask) {}
void glPushClientAttrib(GLbitfield mask) {}
void glTexCoord2f(GLfloat s, GLfloat t) {}
void glVertex2i(GLint x, GLint y) {}
