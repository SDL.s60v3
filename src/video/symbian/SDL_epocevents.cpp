#include "epoc_sdl.h"
#include <stdio.h>
#undef NULL
extern "C" {
#include "SDL_error.h"
#include "SDL_video.h"
#include "SDL_keysym.h"
#include "SDL_keyboard.h"
#include "SDL_events_c.h"
#include "SDL_timer.h"
} /* extern "C" */

#include "SDL_epocvideo.h"
#include "SDL_epocevents_c.h"
#include "sdlepocapi.h"
#include <eikenv.h>
#include <bautils.h>

extern "C"
{
	static SDL_keysym *TranslateKey(_THIS, int scancode, SDL_keysym *keysym);
}

/* The translation tables from a console scancode to a SDL keysym */
static SDLKey keymap[MAX_SCANCODE];
static SDL_keysym *TranslateKey(_THIS, int scancode, SDL_keysym *keysym);

SDLKey* KeyMap()
{
	return keymap;
}

void ResetKeyMap()
{
	/* Initialize the key translation table */
	for( unsigned int i=0; i<SDL_TABLESIZE(keymap); ++i )
		keymap[i] = SDLK_UNKNOWN;

	/* Numbers */
	for( unsigned int i = 0; i<32; ++i )
		keymap[' ' + i] = (SDLKey)(SDLK_SPACE+i);

	/* e.g. Alphabet keys */
	for( unsigned int i = 0; i<32; ++i )
		keymap['A' + i] = (SDLKey)(SDLK_a+i);

	keymap[EStdKeyBackspace]	= SDLK_BACKSPACE;
	keymap[EStdKeyTab]		= SDLK_TAB;
	keymap[EStdKeyEnter]		= SDLK_RETURN;
	keymap[EStdKeyEscape]		= SDLK_ESCAPE;
	keymap[EStdKeySpace]		= SDLK_SPACE;
	keymap[EStdKeyPause]		= SDLK_PAUSE;
	keymap[EStdKeyHome]		= SDLK_HOME;
	keymap[EStdKeyEnd]		= SDLK_END;
	keymap[EStdKeyPageUp]		= SDLK_PAGEUP;
	keymap[EStdKeyPageDown]		= SDLK_PAGEDOWN;
	keymap[EStdKeyDelete]		= SDLK_DELETE;
	keymap[EStdKeyUpArrow]		= SDLK_UP;
	keymap[EStdKeyDownArrow]	= SDLK_DOWN;
	keymap[EStdKeyLeftArrow]	= SDLK_LEFT;
	keymap[EStdKeyRightArrow]	= SDLK_RIGHT;
	keymap[EStdKeyCapsLock]		= SDLK_CAPSLOCK;
	keymap[EStdKeyLeftShift]	= SDLK_LSHIFT;
	keymap[EStdKeyRightShift]	= SDLK_RSHIFT;
	keymap[EStdKeyLeftAlt]		= SDLK_LALT;
	keymap[EStdKeyRightAlt]		= SDLK_RALT;
	keymap[EStdKeyLeftCtrl]		= SDLK_LCTRL;
	keymap[EStdKeyRightCtrl]	= SDLK_RCTRL;
	keymap[EStdKeyLeftFunc]		= SDLK_LMETA;
	keymap[EStdKeyRightFunc]	= SDLK_RMETA;
	keymap[EStdKeyInsert]		= SDLK_INSERT;
	keymap[EStdKeyComma]		= SDLK_COMMA;
	keymap[EStdKeyFullStop]		= SDLK_PERIOD;
	keymap[EStdKeyForwardSlash]	= SDLK_SLASH;
	keymap[EStdKeyBackSlash]	= SDLK_BACKSLASH;
	keymap[EStdKeySemiColon]	= SDLK_SEMICOLON;
	keymap[EStdKeySingleQuote]	= SDLK_QUOTE;
	keymap[EStdKeyHash]		= SDLK_HASH;
	keymap[EStdKeySquareBracketLeft]	= SDLK_LEFTBRACKET;
	keymap[EStdKeySquareBracketRight]	= SDLK_RIGHTBRACKET;
	keymap[EStdKeyMinus]		= SDLK_MINUS;
	keymap[EStdKeyEquals]		= SDLK_EQUALS;

	keymap[EStdKeyF1]		= SDLK_F1;
	keymap[EStdKeyF2]		= SDLK_F2;
	keymap[EStdKeyF3]		= SDLK_F3;
	keymap[EStdKeyF4]		= SDLK_F4;
	keymap[EStdKeyF5]		= SDLK_F5;
	keymap[EStdKeyF6]		= SDLK_F6;
	keymap[EStdKeyF7]		= SDLK_F7;
	keymap[EStdKeyF8]		= SDLK_F8;
	keymap[EStdKeyF9]		= SDLK_F9;
	keymap[EStdKeyF10]		= SDLK_F10;
	keymap[EStdKeyF11]		= SDLK_F11;
	keymap[EStdKeyF12]		= SDLK_F12;

	keymap[EStdKeyXXX]		= SDLK_RETURN;		// "fire" key
	keymap[EStdKeyDevice3]		= SDLK_RETURN;		// "fire" key
	keymap[EStdKeyNkpAsterisk]	= SDLK_ASTERISK;
	keymap[EStdKeyYes]		= SDLK_HOME;		// "call" key
	keymap[EStdKeyNo]		= SDLK_END;		// "end call" key
	keymap[EStdKeyDevice0]		= SDLK_SPACE;		// right menu key
	keymap[EStdKeyDevice1]		= SDLK_ESCAPE;		// left menu key
	keymap[EStdKeyDevice2]		= SDLK_POWER;		// power key

	keymap[EStdKeyMenu]		= SDLK_MENU;		// menu key
	keymap[EStdKeyDevice6]		= SDLK_LEFT;		// Rocker (joystick) left
	keymap[EStdKeyDevice7]		= SDLK_RIGHT;		// Rocker (joystick) right
	keymap[EStdKeyDevice8]		= SDLK_UP;		// Rocker (joystick) up
	keymap[EStdKeyDevice9]		= SDLK_DOWN;		// Rocker (joystick) down
	keymap[EStdKeyLeftFunc]		= SDLK_LALT;
	keymap[EStdKeyRightFunc]	= SDLK_RALT;
	keymap[EStdKeyDeviceA]		= SDLK_RETURN;		// "fire" key

	keymap[EStdKeyNumLock]		= SDLK_NUMLOCK;
	keymap[EStdKeyScrollLock]	= SDLK_SCROLLOCK;

	keymap[EStdKeyNkpForwardSlash]	= SDLK_KP_DIVIDE;
	keymap[EStdKeyNkpAsterisk]	= SDLK_KP_MULTIPLY;
	keymap[EStdKeyNkpMinus]		= SDLK_KP_MINUS;
	keymap[EStdKeyNkpPlus]		= SDLK_KP_PLUS;
	keymap[EStdKeyNkpEnter]		= SDLK_KP_ENTER;
	keymap[EStdKeyNkp1]		= SDLK_KP1;
	keymap[EStdKeyNkp2]		= SDLK_KP2;
	keymap[EStdKeyNkp3]		= SDLK_KP3;
	keymap[EStdKeyNkp4]		= SDLK_KP4;
	keymap[EStdKeyNkp5]		= SDLK_KP5;
	keymap[EStdKeyNkp6]		= SDLK_KP6;
	keymap[EStdKeyNkp7]		= SDLK_KP7;
	keymap[EStdKeyNkp8]		= SDLK_KP8;
	keymap[EStdKeyNkp9]		= SDLK_KP9;
	keymap[EStdKeyNkp0]		= SDLK_KP0;
	keymap[EStdKeyNkpFullStop]	= SDLK_KP_PERIOD;

	keymap[EStdKeyDecVolume]	= SDLK_VOLDOWN;
	keymap[EStdKeyIncVolume]	= SDLK_VOLUP;
}

int EPOC_HandleWsEvent(_THIS, const TWsEvent& aWsEvent)
{
	int posted = 0;
	SDL_keysym keysym;

	switch (aWsEvent.Type())
	{
		case EEventSwitchOff:
			SDL_PrivateQuit();
			break;

		case EEventKeyDown: /* Key events */
			posted += SDL_PrivateKeyboard(SDL_PRESSED, TranslateKey(_this, aWsEvent.Key()->iScanCode, &keysym));
			break;

		case EEventKeyUp: /* Key events */
			posted += SDL_PrivateKeyboard(SDL_RELEASED, TranslateKey(_this, aWsEvent.Key()->iScanCode, &keysym));
			break;

		case EEventFocusGained: /* SDL window got focus */
			Private->iIsWindowFocused = true;
			posted += SDL_PrivateAppActive(1, SDL_APPINPUTFOCUS|SDL_APPMOUSEFOCUS);
			break;

		case EEventFocusLost: /* SDL window lost focus */
			Private->iIsWindowFocused = false;
			posted += SDL_PrivateAppActive(0, SDL_APPINPUTFOCUS|SDL_APPMOUSEFOCUS);
			break;

		case EEventModifiersChanged:
			{
				TModifiersChangedEvent* modEvent = aWsEvent.ModifiersChanged();
				TUint modstate = KMOD_NONE;
				if (modEvent->iModifiers == EModifierLeftShift)
					modstate |= KMOD_LSHIFT;
				if (modEvent->iModifiers == EModifierRightShift)
					modstate |= KMOD_RSHIFT;
				if (modEvent->iModifiers == EModifierLeftCtrl)
					modstate |= KMOD_LCTRL;
				if (modEvent->iModifiers == EModifierRightCtrl)
					modstate |= KMOD_RCTRL;
				if (modEvent->iModifiers == EModifierLeftAlt)
					modstate |= KMOD_LALT;
				if (modEvent->iModifiers == EModifierRightAlt)
					modstate |= KMOD_RALT;
				if (modEvent->iModifiers == EModifierLeftFunc)
					modstate |= KMOD_LMETA;
				if (modEvent->iModifiers == EModifierRightFunc)
					modstate |= KMOD_RMETA;
				if (modEvent->iModifiers == EModifierCapsLock)
					modstate |= KMOD_CAPS;
				SDL_SetModState(STATIC_CAST(SDLMod,(modstate | KMOD_LSHIFT)));
			}
			break;

		case EEventPointer:
			{
				TPointerEvent* ptrEvent = aWsEvent.Pointer();

				posted += SDL_PrivateMouseMotion(0, 0, ptrEvent->iPosition.iX, ptrEvent->iPosition.iY);

				switch(ptrEvent->iType)
				{
					case TPointerEvent::EButton1Down:
						posted += SDL_PrivateMouseButton(SDL_PRESSED, SDL_BUTTON_LEFT, 0, 0);
						break;

					case TPointerEvent::EButton1Up:
						posted += SDL_PrivateMouseButton(SDL_RELEASED, SDL_BUTTON_LEFT, 0, 0);
						break;

					default:
						break;
				}
			}
			break;

		default:
			break;
	} 

	return posted;
}

extern "C"
{

	void EPOC_PumpEvents(_THIS)
	{
		CEventQueue* events = g_SDL->EventQueue();
		while(events->HasData())
		{
			const TWsEvent event = events->Shift();
			EPOC_HandleWsEvent(_this, event);
		}
	}

	void EPOC_InitOSKeymap(_THIS)
	{
		ResetKeyMap();
	}

	static SDL_keysym *TranslateKey(_THIS, int scancode, SDL_keysym *keysym)
	{
		/* Set the keysym information */

		keysym->scancode = scancode;

		if ((scancode >= MAX_SCANCODE) && ((scancode - ENonCharacterKeyBase + 0x0081) >= MAX_SCANCODE))
		{
			SDL_SetError("Too big scancode");
			keysym->scancode = SDLK_UNKNOWN;
			keysym->mod = KMOD_NONE; 
			return keysym;
		}

		keysym->mod = SDL_GetModState();

		/* Handle function keys: F1, F2, F3 ... */
		if (keysym->mod & KMOD_META)
		{
			if (scancode >= 'A' && scancode < ('A' + 24))
			{ /* first 32 alphabet keys */
				switch(scancode)
				{
					case 'Q': scancode = EStdKeyF1; break;
					case 'W': scancode = EStdKeyF2; break;
					case 'E': scancode = EStdKeyF3; break;
					case 'R': scancode = EStdKeyF4; break;
					case 'T': scancode = EStdKeyF5; break;
					case 'Y': scancode = EStdKeyF6; break;
					case 'U': scancode = EStdKeyF7; break;
					case 'I': scancode = EStdKeyF8; break;
					case 'A': scancode = EStdKeyF9; break;
					case 'S': scancode = EStdKeyF10; break;
					case 'D': scancode = EStdKeyF11; break;
					case 'F': scancode = EStdKeyF12; break;
				}
				keysym->sym = keymap[scancode];
			}
		}

		if (scancode >= ENonCharacterKeyBase)
		{
			// Non character keys
			keysym->sym = keymap[scancode - ENonCharacterKeyBase + 0x0081]; // !!hard coded
		}
		else
		{
			keysym->sym = keymap[scancode];
		}

		/* If UNICODE is on, get the UNICODE value for the key */
		keysym->unicode = 0;

		return(keysym);
	}

} /* extern "C" */
