#ifndef PROFILE_WATCHER_H
#define PROFILE_WATCHER_H

#include <e32base.h>
#include <profileenginesdkcrkeys.h>
#include <centralrepository.h>

#include "sdlepocapi.h"

class ProfileWatcher : public CActive
{
	public:
		ProfileWatcher(MProfileWatcher& observer);
		~ProfileWatcher();

		int GetProfile();

	private:
		void RunL();
		void DoCancel();

		CRepository* m_repository;
		MProfileWatcher& m_observer;
};

#endif
