extern "C" {
#include "SDL_events_c.h"
}
#include "epoc_sdl.h"
#include "sdlepocapi.h"
#include <e32base.h>
#include <estlib.h>
#include <stdio.h>
#include <badesca.h>
#include <w32std.h>
#include <aknappui.h>
#include <aknapp.h>
#include "SDL_epocevents_c.h"
#include "SDL_keysym.h"
#include "dsa.h"
#include "SDL_loadso.h"
#include <remconcoreapitargetobserver.h>
#include <remconinterfaceselector.h>
#include <remconcoreapitarget.h>
#include "ProfileWatcher.h"

CSDL* g_SDL = NULL;

class CCurrentAppUi;
class CEikonEnv;
class CSdlAppServ;
class CEventQueue;

class EpocSdlEnvData
{
	public:
		void Free();
		void Delete();
		CDsa*                   iDsa;
		CSdlAppServ*            iAppSrv;
		CArrayFix<TSdlCleanupItem>* iCleanupItems;
};

EpocSdlEnvData* gEpocEnv;

static void RunSingleThread()
{
	if(RThread().RequestCount() > 0)
	{
		int err;
		if(CActiveScheduler::RunIfReady(err, CActive::EPriorityIdle))
		{
			CActiveScheduler::Current()->WaitForAnyRequest();
		}
	}
}

int Panic(int aErr, int aLine)
{
	TBuf<64> b;
	b.Format(_L("Main at %d"), aLine);
	User::Panic(b, aErr);
	return 0;
}

bool CEventQueue::HasData()
{
	RunSingleThread();
	return !m_queue.empty();
}

const TWsEvent CEventQueue::Shift()
{
	const TWsEvent event = m_queue.front();
	m_queue.pop();
	return event;
}


TSdlCleanupItem::TSdlCleanupItem(TSdlCleanupOperation aOperation, void* aItem) :
	iOperation(aOperation), iItem(aItem), iThread(RThread().Id())
{}


ProfileWatcher::ProfileWatcher(MProfileWatcher& observer)
	: CActive(EPriorityLow)
	, m_repository(CRepository::NewL(KCRUidProfileEngine))
	, m_observer(observer)
{
	CActiveScheduler::Add(this);
	m_repository->NotifyRequest(KProEngActiveProfile, iStatus);
	SetActive();
}

ProfileWatcher::~ProfileWatcher()
{
	Cancel();
	delete m_repository;
}

int ProfileWatcher::GetProfile()
{
	int value;
	m_repository->Get(KProEngActiveProfile, value);
	return value;
}

void ProfileWatcher::DoCancel()
{
	m_repository->NotifyCancel(KProEngActiveProfile);
}

void ProfileWatcher::RunL()
{
	int value;
	if(m_repository->Get(KProEngActiveProfile, value) == KErrNone)
	{
		m_observer.ProfileChanged(value);
	}
	m_repository->NotifyRequest(KProEngActiveProfile, iStatus);
	SetActive();
}


class CSdlAppServ : public CActive
{
	public:
		CSdlAppServ() : CActive(CActive::EPriorityIdle)
		{
			CActiveScheduler::Add(this);
			SetActive();
		}
		~CSdlAppServ() { Cancel(); }
		void Request()
		{
			TRequestStatus* status = &iStatus;
			User::RequestComplete(status, KErrNone);
		}

	private:
		void RunL() {}
		void DoCancel()
		{
			TRequestStatus* status = &iStatus;
			User::RequestComplete(status, KErrCancel);
		}
};

bool EpocSdlEnv::IsDsaAvailable()
{
	__ASSERT_DEBUG(gEpocEnv != NULL, PANIC(KErrNotReady));
	return gEpocEnv->iDsa != NULL && gEpocEnv->iDsa->IsDsaAvailable();
}

int EpocSdlEnv::AllocSurface(int bpp)
{
	return gEpocEnv->iDsa->AllocSurface(bpp);
}

void EpocSdlEnv::UnlockHwSurface()
{
	gEpocEnv->iDsa->UnlockHwSurface();
}

TUint8* EpocSdlEnv::LockHwSurface()
{
	return gEpocEnv->iDsa->LockSurface();
}

bool EpocSdlEnv::Blit(TUint8* aAddress, const TSize& aSize)
{
	return gEpocEnv->iDsa->Blit(aAddress, aSize);
}

int EpocSdlEnv::SetPalette(int aFirstcolor, int aColorCount, TUint32* aPalette)
{
	return 	gEpocEnv->iDsa->SetPalette(aFirstcolor, aColorCount, aPalette);
}

int EpocSdlEnv::AppendCleanupItem(const TSdlCleanupItem& aItem)
{
	TRAPD(err, gEpocEnv->iCleanupItems->AppendL(aItem));
	return err;
}

void EpocSdlEnv::RemoveCleanupItem(void* aItem)
{
	for(int i = 0; i < gEpocEnv->iCleanupItems->Count(); i++)
	{
		if(gEpocEnv->iCleanupItems->At(i).iItem == aItem)
			gEpocEnv->iCleanupItems->Delete(i);
	}
}

void EpocSdlEnv::CleanupItems()
{
	const TThreadId id = RThread().Id();
	int last = gEpocEnv->iCleanupItems->Count() - 1;
	int i;

	for(i = last; i >= 0 ; i--)
	{
		TSdlCleanupItem& item = gEpocEnv->iCleanupItems->At(i);
		if(item.iThread == id)
		{
			item.iThread = TThreadId(0); 
			item.iOperation(item.iItem);
		}
	}

	last = gEpocEnv->iCleanupItems->Count() - 1;

	for(i = last; i >= 0 ; i--)
	{
		TSdlCleanupItem& item = gEpocEnv->iCleanupItems->At(i);
		if(item.iThread == TThreadId(0))
		{
			gEpocEnv->iCleanupItems->Delete(i);
		}
	}
}

void EpocSdlEnv::FreeSurface()
{
	gEpocEnv->iAppSrv->Request();

	if(gEpocEnv->iDsa != NULL)
	{
		gEpocEnv->iDsa->Stop();
		gEpocEnv->iDsa->Free();
	}
}

void EpocSdlEnvData::Free()
{
	if(iDsa != NULL)
		iDsa->Free();
}

void EpocSdlEnvData::Delete()
{
	if(iDsa != NULL)
		iDsa->Free();

	delete iDsa;
	delete iAppSrv;
}

static int MainL()
{
	gEpocEnv->iCleanupItems = new CArrayFixFlat<TSdlCleanupItem>(8);

	char** argv = new char*[1];
	argv[0] = new char[8];
	strcpy(argv[0], "app.exe");

	int ret = SDL_main( 1, argv );

	delete[] argv[0];
	delete[] argv;

	return ret;
}

static int DoMain()
{
	TRAPD(err, err = MainL());

	// Free resources and return
	EpocSdlEnv::CleanupItems();

	gEpocEnv->iCleanupItems->Reset();
	delete gEpocEnv->iCleanupItems;
	gEpocEnv->iCleanupItems = NULL;

	gEpocEnv->Free(); //free up in thread resources 

	return err;
}

CSDL::CSDL()
	: m_orientationWait( false )
	, m_soundPauseReason( SPR_NONE )
	, m_profileWatcher( new ProfileWatcher(*this) )
{
	if(m_profileWatcher->GetProfile() == 1)
	{
		m_soundPauseReason |= SPR_SILENTPROFILE;
	}

	__ASSERT_ALWAYS(gEpocEnv == NULL, PANIC(KErrAlreadyExists));
	gEpocEnv = new EpocSdlEnvData;
	Mem::FillZ(gEpocEnv, sizeof(EpocSdlEnvData));

	m_eventQueue = new CEventQueue();

	gEpocEnv->iAppSrv = new CSdlAppServ();

	g_SDL = this;
}

CSDL::~CSDL()
{
	delete m_profileWatcher;

	g_SDL = NULL;

	delete m_eventQueue;

	gEpocEnv->Free();
	gEpocEnv->Delete();

	delete gEpocEnv;
	gEpocEnv = NULL;
}

void CSDL::SetContainerWindowL(RWindow& aWindow, RWsSession& aSession, CWsScreenDevice& aDevice)
{
	if(gEpocEnv->iDsa == NULL)
		gEpocEnv->iDsa = CDsa::CreateL(aSession);
	gEpocEnv->iDsa->ConstructL(aWindow, aDevice);
}

int EpocSdlEnv::ApplyGlesDsa()
{
	CDsa* dsa = NULL;
	TRAPD(err, dsa = gEpocEnv->iDsa->CreateGlesDsaL());
	gEpocEnv->iDsa = dsa;
	return err;
}

RWindow* EpocSdlEnv::Window()
{
	return gEpocEnv->iDsa->Window();
}

void CSDL::CallMainL()
{
	ASSERT(gEpocEnv != NULL);

	// for handling volume up/down keys
	CRemConInterfaceSelector *iSelector = CRemConInterfaceSelector::NewL();
	CRemConCoreApiTarget::NewL( *iSelector, *this );
	iSelector->OpenTargetL();

	// when priority is not lowered screen updates much more frequently, which
	// may be undesired, for example in case of openttd's generating world dialog
	RThread().SetPriority(EPriorityLess);
	DoMain();
}

void CSDL::AppendWsEvent(const TWsEvent& aEvent)
{
	m_eventQueue->Append(aEvent);
}

void CSDL::Resize()
{
	if(m_orientationWait)
	{
		EpocSdlEnv::AllocSurface(m_bpp);
		m_orientationWait = false;
	}
	else
	{
		TSize size = gEpocEnv->iDsa->Window()->Size();
		SDL_PrivateResize(size.iWidth, size.iHeight);
	}
}

void CSDL::SetOrientation(CAknAppUi::TAppUiOrientation orientation, int bpp)
{
	m_orientationWait = true;
	m_bpp = bpp;

	TRAPD(err, static_cast<CAknAppUi*>(CEikonEnv::Static()->EikAppUi())->SetOrientationL(orientation));
}

void CSDL::SetFocus(bool focused)
{
	if(focused)
	{
		m_soundPauseReason &= ~SPR_FOCUSLOST;
	}
	else
	{
		m_soundPauseReason |= SPR_FOCUSLOST;
	}
}

void CSDL::MrccatoCommand(TRemConCoreApiOperationId aOperationId, TRemConCoreApiButtonAction aButtonAct)
{
	if(aButtonAct != ERemConCoreApiButtonClick)
		return;

	TWsEvent event;
	event.SetType(EEventKey);
	event.SetTimeNow();

	switch(aOperationId)
	{
	case ERemConCoreApiVolumeDown:
		event.Key()->iScanCode = EStdKeyDecVolume;
		event.SetType(EEventKeyDown);
		AppendWsEvent(event);
		event.SetType(EEventKeyUp);
		AppendWsEvent(event);
		break;

	case ERemConCoreApiVolumeUp:
		event.Key()->iScanCode = EStdKeyIncVolume;
		event.SetType(EEventKeyDown);
		AppendWsEvent(event);
		event.SetType(EEventKeyUp);
		AppendWsEvent(event);
		break;

	default:
		break;
	}
}

void CSDL::ProfileChanged(int profile)
{
	// Check for silent profile
	if(profile == 1)
	{
		m_soundPauseReason |= SPR_SILENTPROFILE;
	}
	else
	{
		m_soundPauseReason &= ~SPR_SILENTPROFILE;
	}
}
