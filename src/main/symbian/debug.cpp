#include "debug.h"
#include <stdio.h>
#include <stdarg.h>

void DEBUG(char* format, ...)
{
	char *buf = new char[1024];

	va_list args;
	va_start(args, format);
	vsprintf(buf, format, args);

	FILE *f = fopen("e:/sdl.txt", "a");
	fprintf(f, "%s\n", buf);
	fclose(f);

	delete[] buf;

	va_end(args);
}
