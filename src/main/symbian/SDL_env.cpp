#include<e32std.h>

void* SDL_LoadObject(const char *sofile)
{
	RLibrary* lib = new RLibrary();
	if(lib == NULL)
		return NULL;
	TFileName name;
	name.Copy(TPtrC8((const TUint8*)sofile));
	if(KErrNone == lib->Load(name))
		return lib;
	delete lib;
	return NULL;
}

void* SDL_LoadFunction(void *handle, const char *name)
{
	TLex8 v((const TUint8*)(name));
	int ord;

	if(KErrNone != v.Val(ord))
		return NULL;

	const RLibrary* lib = reinterpret_cast<RLibrary*>(handle);
	TLibraryFunction f = lib->Lookup(ord);
	return (void*)(f); 
}

void SDL_UnloadObject(void *handle)	
{
	RLibrary* lib = reinterpret_cast<RLibrary*>(handle);
	lib->Close();
	delete lib; 
}
