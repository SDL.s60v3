#include <aknapp.h>
#include <aknappui.h>
#include <eikdoc.h>
#include <sdlepocapi.h>
#include <bautils.h>
#include <eikstart.h>
#include <badesca.h>
#include <bautils.h>
#include <apgcli.h>
#include <eikedwin.h>
#include <eiklabel.h>
#include <aknglobalmsgquery.h>
#include <apgwgnam.h>

class CApaDocument;

class Ticker : public CActive
{
	public:
		Ticker() : CActive(CActive::EPriorityIdle)
		{
			CActiveScheduler::Add(this);
			SetActive();
		}
		~Ticker() { Cancel(); }

	private:
		void RunL()	{}
		void DoCancel()	{}
};

////////////////////////////////////////////////////////////////////////

class CSDLWin : public CCoeControl
{
	public:
		CSDLWin( const TRect& aRect );
		RWindow& GetWindow() const	{ return Window(); }

	private:
		void Draw(const TRect& aRect) const;
		void HandlePointerEventL(const TPointerEvent& aPointerEvent);
};

CSDLWin::CSDLWin( const TRect& aRect )
{
	CreateWindowL();
	SetRect(aRect);
	EnableDragEvents();
	ActivateL();
	Window().SetRequiredDisplayMode(EColor64K);
}

void CSDLWin::Draw(const TRect& /*aRect*/) const
{
}

void CSDLWin::HandlePointerEventL(const TPointerEvent& aPointerEvent)
{
	CCoeControl::HandlePointerEventL(aPointerEvent);

	TWsEvent event;
	event.SetType(EEventPointer);
	memcpy(event.Pointer(), &aPointerEvent, sizeof(TPointerEvent));
	g_SDL->AppendWsEvent(event);
}

////////////////////////////////////////////////////////////////////////////

class CSDLAppUi : public CAknAppUi
{
	public:
		~CSDLAppUi();

	private:
		void ConstructL();
		void HandleCommandL(int aCommand);
		void HandleWsEventL(const TWsEvent& aEvent, CCoeControl* aDestination);
		void HandleResourceChangeL(int aType);

		void HandleForegroundEventL(TBool aForeground);

		Ticker* iTicker;
		CSDLWin* iSDLWin;
		CSDL* iSdl;
};

CSDLAppUi::~CSDLAppUi()
{
	delete iTicker;
	delete iSdl;
	delete iSDLWin;
}

void CSDLAppUi::ConstructL()
{
	BaseConstructL(ENoAppResourceFile|EAknEnableSkin);

	iSDLWin = new CSDLWin(ApplicationRect());
	iSdl = new CSDL;
	iTicker = new Ticker;

	SetKeyBlockMode(ENoKeyBlock);

	iSdl->SetContainerWindowL(iSDLWin->GetWindow(), iEikonEnv->WsSession(), *iEikonEnv->ScreenDevice());
	iSdl->CallMainL();
	Exit();
}

void CSDLAppUi::HandleCommandL(int aCommand)
{
	switch(aCommand)
	{
		case EAknSoftkeyBack:
		case EAknSoftkeyExit:
		case EAknCmdExit:
		case EEikCmdExit:
		{
			TWsEvent event;
			event.SetType(EEventSwitchOff), event.SetTimeNow();
			iSdl->AppendWsEvent(event);
			break;
		}

		default:
			break;
	}
}

void CSDLAppUi::HandleWsEventL(const TWsEvent& aEvent, CCoeControl* aDestination)
{
	if(aEvent.Type() == KAknUidValueEndKeyCloseEvent)
	{
		TWsEvent event;
		event.SetType(EEventSwitchOff), event.SetTimeNow();
		iSdl->AppendWsEvent(event);
		return;
	}
	else
	{
		iSdl->AppendWsEvent(aEvent);
	}

	CAknAppUi::HandleWsEventL(aEvent, aDestination);
}

void CSDLAppUi::HandleResourceChangeL(int aType)
{
	CAknAppUi::HandleResourceChangeL(aType);
	if(aType == KEikDynamicLayoutVariantSwitch)
	{
		iSDLWin->SetRect(ApplicationRect());
		iSdl->SetContainerWindowL(iSDLWin->GetWindow(), iEikonEnv->WsSession(), *iEikonEnv->ScreenDevice());
		iSdl->Resize();
	}
}

void CSDLAppUi::HandleForegroundEventL(TBool aForeground)
{
	CAknAppUi::HandleForegroundEventL(aForeground);
	g_SDL->SetFocus(aForeground);
}

////////////////////////////////////////////////////////////////////////////

class CSDLDocument : public CEikDocument
{
	public:
		CSDLDocument(CEikApplication& aApp) : CEikDocument(aApp) {}

	private:
		CEikAppUi* CreateAppUiL() { return new CSDLAppUi; }
};

////////////////////////////////////////////////////////////////////////

class CSDLApplication : public CAknApplication
{
	public:
		CSDLApplication();

	private:
		CApaDocument* CreateDocumentL()	{ return new CSDLDocument(*this); }
		TUid AppDllUid() const		{ return iUid; }

		TUid iUid;
};

CSDLApplication::CSDLApplication()
{
	RApaLsSession apa;
	User::LeaveIfError(apa.Connect());
	CleanupClosePushL(apa);
	User::LeaveIfError(apa.GetAllApps());
	TFileName name = RProcess().FileName();
	TApaAppInfo info;
	while(apa.GetNextApp(info) == KErrNone)
	{
		if(info.iFullName.CompareF(name) == 0)
		{
			iUid = info.iUid;
			break;
		}
	}
	CleanupStack::PopAndDestroy();
}

////////////////////////////////////////////////////////////////////////

CApaApplication* NewApplication()
{
	return new CSDLApplication();
}

int E32Main()
{
	return EikStart::RunApplication(NewApplication);
}
