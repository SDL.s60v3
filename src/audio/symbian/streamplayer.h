#ifndef STREAMPLAYER_H
#define STREAMPLAYER_H

#include <mdaaudiooutputstream.h>

const int KMaxVolume = 256;

class MStreamObs
{
	public:
		enum
		{
			EInit,
			EPlay,
			EWrite,
			EClose,
		};
		virtual void Complete(int aState, int aError) = 0;
};

class MStreamProvider
{
	public:
		virtual TPtrC8 Data() = 0;
};

class CStreamPlayer : public CBase, public MMdaAudioOutputStreamCallback
{
	public:
		CStreamPlayer(MStreamProvider& aProvider, MStreamObs& aObs);
		~CStreamPlayer();

		static int ClosestSupportedRate(int aRate);

		int OpenStream(int aRate, int aChannels, TUint32 aType = KMMFFourCCCodePCM16);

		void SetVolume(int aNew);

		void Stop();
		void Start();
		void Open();
		void Close();

		bool Playing() const	{ return (iState & EInited) && (iState & EStarted); }
		bool Closed() const	{ return !(iState & EInited) && !(iState & EDied); }

	private:
		void MaoscOpenComplete(int aError);
		void MaoscBufferCopied(int aError, const TDesC8& aBuffer);
		void MaoscPlayComplete(int aError);

		void Request();

		MStreamProvider& iProvider;
		MStreamObs& iObs;
		int iVolume;

		CMdaAudioOutputStream* iStream;

		int iRate;
		int iChannels;
		TUint32 iType;

		enum
		{
			ENone		= 0,
			EInited		= 0x1,
			EStarted	= 0x2,
			EStopped	= 0x4,
			EVolumeChange	= 0x8,
			EDied		= 0x10
		};

		int iState;
		TPtrC8 iPtr;
};

#endif
