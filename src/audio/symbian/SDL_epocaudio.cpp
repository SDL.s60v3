/*
   SDL - Simple DirectMedia Layer
   Copyright (C) 1997, 1998, 1999, 2000, 2001  Sam Lantinga

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this library; if not, write to the Free
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

   Sam Lantinga
   slouken@devolution.com
   */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/stat.h>

#include "epoc_sdl.h"
#include "sdlepocapi.h"

#include <e32hal.h>


extern "C" {
#include "SDL_audio.h"
#include "SDL_error.h"
#include "SDL_audiomem.h"
#include "SDL_audio_c.h"
#include "SDL_timer.h"
#include "SDL_audiodev_c.h"
#include "SDL_sysaudio.h"
}

#include "streamplayer.h"

/* Audio driver functions */

static int EPOC_OpenAudio(SDL_AudioDevice *thisdevice, SDL_AudioSpec *spec);
static void EPOC_WaitAudio(SDL_AudioDevice *thisdevice);
static void EPOC_PlayAudio(SDL_AudioDevice *thisdevice);
static Uint8 *EPOC_GetAudioBuf(SDL_AudioDevice *thisdevice);
static void EPOC_CloseAudio(SDL_AudioDevice *thisdevice);
static void EPOC_ThreadInit(SDL_AudioDevice *thisdevice);

static int Audio_Available(void);
static SDL_AudioDevice *Audio_CreateDevice(int devindex);
static void Audio_DeleteDevice(SDL_AudioDevice *device);

class CSimpleWait : public CTimer
{
	public:
		CSimpleWait();

	private:
		void RunL();
};

CSimpleWait::CSimpleWait()
	: CTimer(CActive::EPriorityStandard)
{
	CActiveScheduler::Add(this);
	ConstructL();
}

void CSimpleWait::RunL()
{
	CActiveScheduler::Stop();
}

class CEpocAudio : public CBase, public MStreamObs, public MStreamProvider
{
	public:
		CEpocAudio(int aBufferSize, int aFill, int aRate, int aChannels, TUint32 aType);
		~CEpocAudio();

		inline static CEpocAudio& Current(SDL_AudioDevice* thisdevice);

		static void Free(SDL_AudioDevice* thisdevice);

		void Wait();
		void ThreadInitL();
		TUint8* Buffer();

	private:
		void Complete(int aState, int aError);
		TPtrC8 Data();

		CStreamPlayer* iPlayer;
		int iRate;
		int iChannels;
		TUint32 iType;
		int iBufferSize;
		TUint8* iBuffer;
		CSimpleWait* iWait;
};

CEpocAudio::CEpocAudio(int aBufferSize, int aFill, int aRate, int aChannels, TUint32 aType)
	: iRate(aRate)
	, iChannels(aChannels)
	, iType(aType)
	, iBufferSize(aBufferSize)
{
	iBuffer = new TUint8[iBufferSize];
	memset(iBuffer, aFill, iBufferSize);
}

CEpocAudio::~CEpocAudio()
{
	if(iWait != NULL)
	{
		iWait->Cancel();
		delete iWait;
	}
	if(iPlayer != NULL)
	{
		iPlayer->Close();
		delete iPlayer;
	}
	delete iBuffer;
}

inline CEpocAudio& CEpocAudio::Current(SDL_AudioDevice* thisdevice)
{
	return *(CEpocAudio*)thisdevice->hidden;
}

void CEpocAudio::Free(SDL_AudioDevice* thisdevice)
{
	CEpocAudio* ea = (CEpocAudio*)thisdevice->hidden;
	if(ea)
	{
		delete ea;
		thisdevice->hidden = NULL;

		CActiveScheduler* as = CActiveScheduler::Current();
		ASSERT(as->StackDepth() == 0);
		delete as;
		CActiveScheduler::Install(NULL);
	}
	ASSERT(thisdevice->hidden == NULL);
}

void CEpocAudio::ThreadInitL()
{
	iWait = new CSimpleWait;

	iPlayer = new CStreamPlayer(*this, *this);
	iPlayer->OpenStream(iRate, iChannels, iType);

	/// \todo Implement proper start/pause conditions
	iPlayer->Start();
}

TUint8* CEpocAudio::Buffer()
{
	return iBuffer;
}

void CEpocAudio::Complete(int aState, int aError)
{
	if(iPlayer->Closed())
		return;

	switch(aError)
	{
		case KErrUnderflow:
		case KErrInUse:
			iPlayer->Start();
			break;
		case KErrAbort:
			iPlayer->Open();
	}
}

TPtrC8 CEpocAudio::Data()
{
	if(iWait->IsActive())
	{
		iWait->Cancel();
		CActiveScheduler::Stop();
	}

	if(g_SDL->GetSoundPauseReason() != CSDL::SPR_NONE)
	{
		if(iPlayer->Playing())
		{
			iPlayer->Stop();
		}
		return KNullDesC8();
	}

	TPtrC8 data(iBuffer, iBufferSize);

	return data;
}

void CEpocAudio::Wait()
{
	int pauseReason = g_SDL->GetSoundPauseReason();

	if(pauseReason == CSDL::SPR_NONE)
	{
		if(!iPlayer->Playing())
		{
			iPlayer->Start();
		}

		// This wait will be terminated by call to Data() from audio buffer callback.
		SDL_PauseAudio(0);
		iWait->After(10000000);
	}
	else
	{
		// This wait shouldn't be terminated.
		if(pauseReason & CSDL::SPR_HARDSTOP)
		{
			// Game is paused, so don't process audio.
			SDL_PauseAudio(1);
			iWait->After(100000);
		}
		else
		{
			// Game is running with audio disabled.
			// Audio should be mixed in case application expects audio to timer synchronization.
			// Approximate time needed for mixing.
			SDL_PauseAudio(0);
			iWait->After( ( (iBufferSize * 1000) / (iRate * iChannels * 2) ) * 1000 );
		}
	}

	CActiveScheduler::Start();
}

/* Audio driver bootstrap functions */

AudioBootStrap EPOCAudio_bootstrap = {
	"epoc\0\0\0",
	"EPOC streaming audio\0\0\0",
	Audio_Available,
	Audio_CreateDevice
};


static SDL_AudioDevice *Audio_CreateDevice(int /*devindex*/)
{
	SDL_AudioDevice *thisdevice;

	/* Initialize all variables that we clean on shutdown */
	thisdevice = new SDL_AudioDevice;
	if ( thisdevice )
	{
		memset(thisdevice, 0, (sizeof *thisdevice));
		thisdevice->hidden = NULL;
	}
	else
	{
		SDL_OutOfMemory();
		return(0);
	}

	/* Set the function pointers */
	thisdevice->OpenAudio = EPOC_OpenAudio;
	thisdevice->WaitAudio = EPOC_WaitAudio;
	thisdevice->PlayAudio = EPOC_PlayAudio;
	thisdevice->GetAudioBuf = EPOC_GetAudioBuf;
	thisdevice->CloseAudio = EPOC_CloseAudio;
	thisdevice->ThreadInit = EPOC_ThreadInit;
	thisdevice->free = Audio_DeleteDevice;

	return thisdevice;
}

static void Audio_DeleteDevice(SDL_AudioDevice *device)
{
	delete device;
}

static int Audio_Available(void)
{
	return(1); // Audio stream modules should be always there!
}

static int EPOC_OpenAudio(SDL_AudioDevice *thisdevice, SDL_AudioSpec *spec)
{
	TUint32 type = KMMFFourCCCodePCM16;

	switch(spec->format)
	{
		case AUDIO_U16LSB:
			type = KMMFFourCCCodePCMU16;
			break;

		case AUDIO_S16LSB:
			type = KMMFFourCCCodePCM16;
			break;

		case AUDIO_U16MSB:
			type = KMMFFourCCCodePCMU16B;
			break;

		case AUDIO_S16MSB:
			type = KMMFFourCCCodePCM16B;
			break;

		case AUDIO_U8:
			type = KMMFFourCCCodePCMU8;
			break;

		case AUDIO_S8:
			type = KMMFFourCCCodePCM8;
			break;

		default:
			spec->format = AUDIO_S16LSB;
	};

	if(spec->channels > 2)
		spec->channels = 2;

	spec->freq = CStreamPlayer::ClosestSupportedRate(spec->freq);

	/* Allocate mixing buffer */
	const int buflen = spec->size;

	thisdevice->hidden = (SDL_PrivateAudioData*)new CEpocAudio(buflen, spec->silence, spec->freq, spec->channels, type);
	thisdevice->enabled = 0; // enable only after audio engine has been initialized!

	return 0;
}

static void EPOC_CloseAudio(SDL_AudioDevice* thisdevice)
{
	CEpocAudio::Free(thisdevice);
}

static void EPOC_ThreadInit(SDL_AudioDevice *thisdevice)
{
	CActiveScheduler* as =  new CActiveScheduler();
	CActiveScheduler::Install(as);

	EpocSdlEnv::AppendCleanupItem(TSdlCleanupItem((TSdlCleanupOperation)EPOC_CloseAudio, thisdevice));

	CEpocAudio::Current(thisdevice).ThreadInitL();
	RThread().SetPriority(EPriorityMore);
	thisdevice->enabled = 1;
}

/* This function waits until it is possible to write a full sound buffer */
static void EPOC_WaitAudio(SDL_AudioDevice* thisdevice)
{
	CEpocAudio::Current(thisdevice).Wait();
}

static void EPOC_PlayAudio(SDL_AudioDevice* thisdevice)
{
}

static Uint8 *EPOC_GetAudioBuf(SDL_AudioDevice* thisdevice)
{
	return CEpocAudio::Current(thisdevice).Buffer();
}
