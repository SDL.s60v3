#include "streamplayer.h"
#include <mda/common/audio.h>
#include <unistd.h>

static int GetSampleRate(int aRate)
{
	switch(aRate)
	{
		case 8000: return TMdaAudioDataSettings::ESampleRate8000Hz;
		case 11025: return TMdaAudioDataSettings::ESampleRate11025Hz;
		case 12000: return TMdaAudioDataSettings::ESampleRate12000Hz;
		case 16000: return TMdaAudioDataSettings::ESampleRate16000Hz;
		case 22050: return TMdaAudioDataSettings::ESampleRate22050Hz;
		case 24000: return TMdaAudioDataSettings::ESampleRate24000Hz;
		case 32000: return TMdaAudioDataSettings::ESampleRate32000Hz;
		case 44100: return TMdaAudioDataSettings::ESampleRate44100Hz;
		case 48000: return TMdaAudioDataSettings::ESampleRate48000Hz;
		case 64000: return TMdaAudioDataSettings::ESampleRate64000Hz;
		case 96000: return TMdaAudioDataSettings::ESampleRate96000Hz;
		default: break;
	}
	return KErrNotFound;
}

static int GetChannels(int aChannels)
{
	switch(aChannels)
	{
		case 1: return TMdaAudioDataSettings::EChannelsMono;
		case 2: return TMdaAudioDataSettings::EChannelsStereo;
		default: break;
	}
	return KErrNotFound;
}

int CStreamPlayer::ClosestSupportedRate(int aRate)
{
	if(aRate > 96000)
		return 96000;

	int rate = aRate;
	while(GetSampleRate(rate) == KErrNotFound)
	{
		++rate;
	}
	return rate;
}

CStreamPlayer::CStreamPlayer(MStreamProvider& aProvider, MStreamObs& aObs)
	: iProvider(aProvider)
	, iObs(aObs)
	, iVolume(KMaxVolume)
{
	iStream = CMdaAudioOutputStream::NewL(*this, EMdaPriorityMax);
}

CStreamPlayer::~CStreamPlayer()
{
	iState |= EDied;
	if(iState & EInited)
		Close();
	delete iStream;
}

int CStreamPlayer::OpenStream(int aRate, int aChannels, TUint32 aType)
{
	Close();

	iType = aType;

	iRate = GetSampleRate(aRate);
	if(iRate == KErrNotFound)
		return KErrNotSupported;

	iChannels = GetChannels(aChannels);
	if(iChannels == KErrNotFound)
		return KErrNotSupported;

	Open();

	return KErrNone;
}

void CStreamPlayer::SetVolume(int aNew)
{
	if(aNew < 0 || aNew > KMaxVolume)
		return;

	iVolume = aNew;
	iState |= EVolumeChange;
}

void CStreamPlayer::Open()
{
	TMdaAudioDataSettings audioSettings;
	audioSettings.Query();
	audioSettings.iSampleRate = iRate;
	audioSettings.iChannels = iChannels;
	audioSettings.iVolume = 0;

	iState &= ~EStopped;
	iStream->Open(&audioSettings);
}

void CStreamPlayer::Stop()
{
	if(iState & (EStarted | EInited))
	{
		Close();
		iState |= EStopped;
	}
}

void CStreamPlayer::Start()
{
	iState |= EStarted;
	if(iState & EInited)
	{
		Request();
	}
	else if(iState & EStopped)
	{
		Open();
	}
}

void CStreamPlayer::Close()
{
	iState &= ~EInited;
	iStream->Stop();
	iState &= ~EStarted;
}

void CStreamPlayer::Request()
{
	if(iState & EVolumeChange)
	{
		iStream->SetVolume((iVolume * iStream->MaxVolume()) / KMaxVolume);
		iState &= ~EVolumeChange;
	}

	if(iState & EStarted)
	{
		iPtr.Set(iProvider.Data());
	}
	if(iPtr.Length() != 0)
	{
		TRAPD(err, iStream->WriteL(iPtr));
		if(err != KErrNone)
		{
			iObs.Complete(MStreamObs::EWrite, err);
		}
	}
}

void CStreamPlayer::MaoscOpenComplete(int aError)
{
	if(aError == KErrNone)
	{
		iStream->SetDataTypeL(iType);
		iStream->SetAudioPropertiesL(iRate, iChannels);
		iStream->SetPriority(EPriorityNormal, EMdaPriorityPreferenceTime);

		iState |= EInited;

		SetVolume(iVolume);

		if(iState & EStarted)
		{
			Request();
		}
	}
	if(!(iState & EDied))
		iObs.Complete(MStreamObs::EInit, aError);
}

void CStreamPlayer::MaoscBufferCopied(int aError, const TDesC8& /*aBuffer*/)
{
	if(aError == KErrNone)
	{
		if(iState & EInited)
			Request();
		else
			iStream->Stop();
	}
	else if(!(iState & EDied))
		iObs.Complete(MStreamObs::EPlay, aError);
}

void CStreamPlayer::MaoscPlayComplete(int aError)
{
	iState &= ~EStarted;
	if(!(iState & EDied))
		iObs.Complete(MStreamObs::EClose, aError);
}
