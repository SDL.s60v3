#ifndef EPOC_SDL_H
#define EPOC_SDL_H

#include <e32std.h>
#include <remconcoreapitargetobserver.h>
#include "epoc_sdl.h"

class CDesC8Array;
class TWsEvent;
class RWindow;
class CAknAppUi;
class RWsSession;
class CWsScreenDevice;
class CBitmapContext;
class CFbsBitmap;

extern "C"
{
	int SDL_main (int argc, char* argv[]);
}

class ProfileWatcher;
class MProfileWatcher
{
	public:
		virtual void ProfileChanged(int profile) = 0;
};

class CSDL : public MRemConCoreApiTargetObserver, public MProfileWatcher
{
	public:
		enum SoundPauseReason
		{
			SPR_NONE                = 0,
			SPR_FOCUSLOST           = 0x1,
			SPR_SILENTPROFILE       = 0x2
		};

		enum { SPR_HARDSTOP = SPR_FOCUSLOST };

		CSDL();
		virtual ~CSDL();

		void SetContainerWindowL(RWindow& aWindow, RWsSession& aSession, CWsScreenDevice& aDevice);
		void AppendWsEvent(const TWsEvent& aEvent);	//give event to SDL
		void CallMainL();
		void Resize();

		CEventQueue* EventQueue() { return m_eventQueue; }
		void SetOrientation(CAknAppUi::TAppUiOrientation orientation, int bpp);

		int GetSoundPauseReason() const { return m_soundPauseReason; }
		void SetFocus(bool focused);

	private:
		void MrccatoCommand(TRemConCoreApiOperationId aOperationId, TRemConCoreApiButtonAction aButtonAct);
		void ProfileChanged(int profile);

		CEventQueue*	m_eventQueue;
		bool		m_orientationWait;
		int		m_bpp;
		int             m_soundPauseReason;
		ProfileWatcher*	m_profileWatcher;
};

extern CSDL* g_SDL;

#endif
