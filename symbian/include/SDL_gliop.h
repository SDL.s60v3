#ifndef GLIOP_H
#define GLIOP_H

#define GL_VERSION_1_5
#define GL_ES
#define GL_ES_VERSION_1_1

#include <begin_code.h>

#include <GLES/gl.h>

#ifdef __cplusplus
extern "C" {
#endif
/*
These are not defined in GLES
*/
	typedef double GLdouble;
	typedef double GLclampd;
#ifdef __cplusplus
}
#endif	
#include <close_code.h>
#endif
