#ifndef EPOCSDL_H
#define EPOCSDL_H

#include <e32def.h>
#include <e32std.h>
#include <gdi.h>
#include <w32std.h>
#include <aknappui.h>
#include <queue>
#include "SDL_config_symbian.h"

#define PANIC(x) Panic(x, __LINE__)
#define PANIC_IF_ERROR(x)  (x == KErrNone || Panic(x, __LINE__))

TInt Panic(TInt aErr, TInt aLine);

typedef void (*TSdlCleanupOperation) (void* aThis);

class CEventQueue
{
	public:
		void Append(const TWsEvent& aEvent) { m_queue.push(aEvent); }
		const TWsEvent Shift();
		bool HasData();

	private:
		std::queue<TWsEvent> m_queue;
};

class TSdlCleanupItem
{
	public:
		TSdlCleanupItem(TSdlCleanupOperation aOperation, void* aItem);
		TSdlCleanupOperation iOperation;
		void* iItem;
		TThreadId iThread;
};

class EpocSdlEnv
{
	public:
		static bool IsDsaAvailable();
		static TInt AllocSurface(int bpp);
		static void UnlockHwSurface();
		static TUint8* LockHwSurface();
		static bool Blit(TUint8* aAddress, const TSize& aSize);
		static TInt SetPalette(TInt aFirstcolor, TInt aColorCount, TUint32* aPalette);
		static TInt AppendCleanupItem(const TSdlCleanupItem& aItem);
		static void RemoveCleanupItem(void* aItem);
		static void CleanupItems();
		static void FreeSurface();
		static TInt ApplyGlesDsa();
		static RWindow* Window();
};

#endif
