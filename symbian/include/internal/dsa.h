#ifndef __DSA_H__
#define __DSA_H__

#include <e32base.h>
#include <w32std.h>
#include "epoc_sdl.h"
#include "sdlepocapi.h"

class CDsa;

typedef void (*TCopyFunction)(const CDsa& aDsa, TUint32* aTarget, const TUint8* aSource, TInt aBytes);

class CDsa : public CBase
{
	public:
		inline bool IsDsaAvailable() const;

		static CDsa* CreateL(RWsSession& aSession);

		CDsa();
		virtual ~CDsa();

		TInt AllocSurface(int bpp);

		TInt SetPalette(TInt aFirst, TInt aCount, TUint32* aPalette);
		bool Blit(const TUint8* aBits, const TSize& aSize);

		RWsSession& Session();
		RWindow* Window();
		CDsa* CreateGlesDsaL();

		virtual void Free();

		virtual void ConstructL(RWindow& aWindow, CWsScreenDevice& aDevice);

		virtual TUint8* LockSurface() = 0;
		virtual void UnlockHwSurface() = 0;
		virtual void Stop();

		bool m_updateWholeScreen;
		TInt iSourceBpp;
		TUint32* iLut256;

	protected:
		CDsa(RWsSession& aSession);
		void Start();

		CFbsBitmap* iBmp;

	private:
		virtual void CreateSurfaceL() = 0;

		void ClipCopy(TUint8* aTarget, const TUint8* aSource, const TRect& aUpdateRect, const TRect& aSourceRect) const;
		static void Copy256(const CDsa& aDsa, TUint32* aTarget, const TUint8* aSource, TInt aBytes);
		static void CopySlow(const CDsa& aDsa, TUint32* aTarget, const TUint8* aSource, TInt aBytes);
		static void CopyMem(const CDsa& aDsa, TUint32* aTarget, const TUint8* aSource, TInt aBytes);

		void SetCopyFunction();

		bool iRunning;
		RWsSession& iSession;

		TInt iTargetBpp;
		TCopyFunction iCopyFunction;
		TUint8* iTargetAddr;
		TCopyFunction iCFTable[3];
		TInt iNewFlags;
		RWindow* iWindow;
};

inline bool CDsa::IsDsaAvailable() const
{
	return iRunning;
}

#endif	
